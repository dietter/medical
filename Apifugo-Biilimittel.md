# L�sung Nr. 6364

### Zutaten:
|ml|Zutat|
|------:|:------|
|~~8.0~~ 	|	~~Ac. benzoicum~~ (nur noch in fester Form erh�ltlich)|
|8.0 		|	Camphora|
|2.0 		|	Levomenth.|
|~~10.0~~ 	|	~~Naphtalium~~ (nicht mehr erh�ltlich)|
|15.0 		|	Methylis salicylas|
|30.0 		|	Caryoph aeth.|
|30.0 		|	Citr. aeth.|
|30.0 		|	Eucal. aeth.|
|84.0 		|	Ammonii hydro sol. 12%|
|bis 1000.0 	|	mit "Eth. c.camp 96%" auff�llen|
|||
|**217 + 783**|** = 1000**|

**Hofapotheke, Rapperswil**

_05.09.1996_



***
***
***


# L�sung Nr. 6364 (Original)

### Zutaten:
* ~~Ac. benzoicum 8.0~~ (no longer sold)
* Camphora 8.0
* Levomenth. 2.0
* ~~Naphtalium 10.0~~ (no longer sold)	
* Methylis salicylas 15.0
* Caryoph aeth. 30.0
* Citr. aeth. 30.0
* Eucal. aeth. 30.0
* Ammonii hydro sol. 12% 84.0
* Eth. c.camp 96% ad 1000.0

***

**Hofapotheke, Rapperswil**

_05.09.1996_